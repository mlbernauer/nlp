#!/usr/bin/python
from Bio import Medline
import sys
import csv

recs = Medline.parse(open(sys.argv[1]))
output = csv.writer(open(sys.argv[2], 'wb'), delimiter=',', quotechar='"')

for i in recs:
    try:

        output.writerow([i['AU'][-1], i['AB'], i['JT']])
    except:
        print "error"
